var renderer, scene, camera

var init = () => {
  console.clear()

  let [pw, ph, pd] = [
    document.getElementById('pw').value,
    document.getElementById('ph').value,
    document.getElementById('pd').value
  ]
  let [dw, dh, diagonal] = [
    document.getElementById('dw').value,
    document.getElementById('dh').value,
    document.getElementById('diagonal').value
  ]
  let format = findFormat(dw, dh)
  console.log("Running", dw, dh, diagonal, format)

  let ppmm = calc(dw, format, diagonal)

  draw(pw, ph, pd, ppmm)
}

var findFormat = (a, b) => {
  let nwd = calcNWD(a, b)
  return [a/nwd, b/nwd]
}

/**
  Calculates NWD using Euclides method.
 */
var calcNWD = (a, b) => {
  let nwd = 1
  while(a - b !== 0) {
    [a, b] = a > b ? [a - b, b] : [b - a, a]
    nwd = a
  }
  return nwd
}

var getPrime = (iter) => {
  if (iter === 0)
    return 1
  if (iter === 1)
    return 2
  return 3
}

var calc = (dw, format, diagonal) => {
  let [f1, f2] = format
  let a, b, c
  c = diagonal * 25.4 // mm
  b = Math.sqrt( (c*c) / ( (f1 / f2)*(f1 / f2) + 1 ))
  a = (f1 / f2) * b
  let ppmm = dw / a
  console.table({a: a, b: b, c: c, ppmm: ppmm})
  return ppmm
}

var render = () => {
  camera.lookAt( scene.position )
  renderer.render( scene, camera )
}

var onWindowResize = () => {
  render()
}

var draw = (pw, ph, pd, ppmm) => {
  let [rw, rh] = [1200, 1000]

  // renderer
  renderer = new THREE.WebGLRenderer({ antialias: true })
  renderer.setPixelRatio( window.devicePixelRatio )
  renderer.setSize( rw, rh )

  // add to DOM
  let container = document.getElementById('container')
  container.innerHTML = ''
  container.appendChild( renderer.domElement )

  // scene
  scene = new THREE.Scene()
  scene.background = new THREE.Color( 0xf0f0f0 )

  // camera
  camera = new THREE.OrthographicCamera(-rw/2, rw/2, rh/2, -rh/2)
  //camera = new THREE.PerspectiveCamera()
  camera.position.set(rh/2, rh/2, rh/2)

  // grid
  //let gridHelper = new THREE.GridHelper( rh, 20 )
  //scene.add( gridHelper )

  // axes
  //let axesHelper = new THREE.AxesHelper();
  //axesHelper.scale.set(200, 200, 200);
  //scene.add( axesHelper );

  // object
  let geometry = new THREE.BoxGeometry( pw*ppmm, ph*ppmm, pd*ppmm )
  let material = new THREE.MeshNormalMaterial()
  let mesh = new THREE.Mesh( geometry, material )
  scene.add( mesh )

  // controls
  let controls = new THREE.OrbitControls(camera, renderer.domElement)
  controls.addEventListener( 'change', render )
  controls.enablePan = false
  controls.enableZoom = false

  window.addEventListener( 'resize', onWindowResize, false )

  render()
}

document.getElementById('run-btn').addEventListener('click', init)
console.log('App started')